import { Component } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { LugaresService } from '../services/lugares.service';

@Component({
  selector: 'app-lugares',
  templateUrl: './lugares.component.html',
  animations: [
    trigger('contenedorAnimable', [
      state('inicial', style({
        opacity: 0,
      })),
      state('final', style({
        opacity: 1,
      })),
      transition('inicial => final', animate(2000)),
      transition('final => inicial', animate(1000))
    ])
  ]
})
export class LugaresComponent {
  title = 'PlatziSquare';
  state = 'inicial';

  lat:number = 6.2528035;
  lng:number = -75.5991008;
  lugares = null;
  animar(){
    this.state = (this.state === 'final') ? 'inicial' : 'final';
  }

  animacionInicia(e){
    console.log("Iniciado");
    console.log(e);
  }

  animacionTermina(e){
    console.log("terminado");
    console.log(e);
  }

  constructor(private lugaresService:LugaresService){
    lugaresService.getLugares().subscribe(lugares => {
      this.lugares = lugares;
      this.lugares = Object.keys(this.lugares).map((key) => this.lugares[key]);
      this.state="final"
    }, error => {
      console.log("error");
      alert("tenemos algo de dificultades, Error: " + error.statusText);
    });
  }
}
