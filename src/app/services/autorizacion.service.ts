import { Injectable } from "@angular/core";
import { AngularFireAuth } from "angularfire2/auth";
import { Router } from "@angular/router";

@Injectable()
export class AutorizacionService{

    constructor(private angularFireAuth:AngularFireAuth, private router:Router){
        this.isLogged();
    }

    public login = (email, password) => {
        this.angularFireAuth.auth.signInWithEmailAndPassword(email, password).then((response) => {
            alert("Usuario Loggeado con exito!")
            console.log(response);
            this.router.navigate(['lugares']);
        }).catch((error) => {
            alert("Un error ha ocurrido: ")
        })
    }

    public registro = (email, password) => {
        this.angularFireAuth.auth.createUserWithEmailAndPassword(email, password).then((response) => {
            alert("Usuario registrado con exito!")
            console.log(response);
            this.router.navigate(['lugares']);
        }).catch((error) => {
            alert("Un error ha ocurrido: ")
        })
    }

    public isLogged(){
        return this.angularFireAuth.authState;
    }

    public logout(){
        this.angularFireAuth.auth.signOut();
        alert("Sesion cerrada");
        this.router.navigate(['login']);
    }

    public getUser(){
        return this.angularFireAuth.auth;
    }
}